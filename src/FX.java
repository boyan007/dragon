import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FX extends Application {
    int a = 0;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(10);
        gridPane.setHgap(15);

        ImageView source = new ImageView();
        source.setFitWidth(100);
        source.setFitHeight(100);
        source.setSmooth(true);
        source.setImage(new Image("1.jpg"));


        source.setOnDragDetected(event -> {
            Dragboard dragboard = source.startDragAndDrop(TransferMode.ANY);

            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putImage(source.getImage());
            dragboard.setContent(clipboardContent);

            event.consume();
        });
        source.setOnDragDone(e -> {
            if (a % 2 == 0) {
                source.setImage(new Image("2.jpg"));
            } else {
                source.setImage(new Image("1.jpg"));
            }
            a++;
            e.consume();
        });


        ImageView target = new ImageView();
        target.setFitHeight(100);
        target.setFitWidth(100);
        target.setSmooth(true);
        target.setImage(new Image("2.jpg"));

        target.setOnDragOver(e -> {
            if (e.getGestureSource() != target && e.getDragboard().hasImage()) {
                e.acceptTransferModes(TransferMode.ANY);
            }
            e.consume();
        });
        target.setOnDragDropped(e -> {
            Dragboard dragboard = e.getDragboard();

            if (dragboard.hasImage()) {
                target.setImage(dragboard.getImage());
            }
            e.setDropCompleted(true);
            e.consume();
        });

        gridPane.add(source, 0, 0);
        gridPane.add(target, 0, 10);


        Scene scene = new Scene(gridPane, 600, 600);
        primaryStage.setTitle("My little game");
        primaryStage.setScene(scene);
        setUserAgentStylesheet(STYLESHEET_CASPIAN);
        primaryStage.show();
    }
}